import { Component } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';

import { RegisterPage } from '../register/register';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  public formGroup: FormGroup;
  public formControls: { [key: string]: AbstractControl };
  public staticModel: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.formGroup = new FormGroup({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl(null, Validators.compose([
        Validators.required,
        Validators.minLength(9)
      ]))
    });

    this.formControls = this.formGroup.controls;

    this.staticModel = {
      cancelText: 'Закрыть',
      doneText: 'Выбрать',
      errorMessages: {
        required: 'Данное поле обязательно к заполнению',
        email: 'Введен неверный адрес электронной почты',
        minLength: 'Пароль должен быть от 8 знаков и больше'
      }
    };
  }

  public onSubmit() {

  }

  public gotoRegister() {
    return this.navCtrl.setRoot(RegisterPage);
  }

  public forgotPassword() {

  }

}
