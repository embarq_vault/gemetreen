import { Component } from '@angular/core';
import { AbstractControl, FormGroup, FormControl, Validators } from '@angular/forms';
import { NavController, NavParams } from 'ionic-angular';

import { LoginPage } from '../login/login';

@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage {
  public staticModel: any;
  public formGroup: FormGroup;
  public formControls: { [key: string]: AbstractControl };

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams) {
    this.formGroup = new FormGroup({
      email        : new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+$')
      ])),
      password     : new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(8)])),
      phoneNumber  : new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^\\+?\\d+$')
      ])),
      firstName    : new FormControl('', Validators.required),
      lastName     : new FormControl(''),
      patronymic   : new FormControl(''),
      dob          : new FormControl(''),
      country      : new FormControl('', Validators.required),
      city         : new FormControl(''),
      status       : new FormControl('', Validators.required),
      distributorId: new FormControl('', Validators.required),
      dateAttended : new FormControl('', Validators.required)
    });
    this.formControls = this.formGroup.controls;

    this.staticModel = {
      cancelText: 'Закрыть',
      doneText: 'Выбрать',
      errorMessages: {
        required: 'Данное поле обязательно к заполнению',
        email: 'Введен неверный адрес электронной почты',
        password: 'Пароль должен быть от 8 знаков и больше',
        phoneNumber: 'Введен неверный номер телефона'
      },
      countries: [
        {
          value: 'australia',
          label: 'Австралия',
        },
        {
          value: 'azerbajan',
          label: 'Азербайджан',
        },
        {
          value: 'england',
          label: 'Англия',
        },
        {
          value: 'belgium',
          label: 'Бельгия',
        },
        {
          value: 'belorus',
          label: 'Белоруссия',
        },
        {
          value: 'germany',
          label: 'Германия',
        },
        {
          value: 'grece',
          label: 'Греция',
        },
        {
          value: 'georgia',
          label: 'Грузия',
        },
        {
          value: 'israel',
          label: 'Израиль',
        },
        {
          value: 'india',
          label: 'Индия',
        },
        {
          value: 'irland',
          label: 'Ирландия',
        },
        {
          value: 'italy',
          label: 'Италия',
        },
        {
          value: 'kazachstan',
          label: 'Казахстан',
        },
        {
          value: 'canada',
          label: 'Канада',
        },
        {
          value: 'ciprus',
          label: 'Кипр',
        },
        {
          value: 'latvia',
          label: 'Латвия',
        },
        {
          value: 'litva',
          label: 'Литва',
        },
        {
          value: 'mexico',
          label: 'Мексика',
        },
        {
          value: 'moldova',
          label: 'Молдавия',
        },
        {
          value: 'poland',
          label: 'Польша',
        },
        {
          value: 'russia',
          label: 'Россия',
        },
        {
          value: 'romany',
          label: 'Румыния',
        },
        {
          value: 'usa',
          label: 'США',
        },
        {
          value: 'uzbeckistan',
          label: 'Узбекистан',
        },
        {
          value: 'ukraine',
          label: 'Украина',
        },
        {
          value: 'finland',
          label: 'Финляндия',
        },
        {
          value: 'france',
          label: 'Франция',
        },
        {
          value: 'chehia',
          label: 'Чехия',
        },
        {
          value: 'estonia',
          label: 'Эстония',
        },
        {
          value: 'spain',
          label: 'Испания',
        },
        {
          value: 'kirgistan',
          label: 'Киргизстан',
        },
        {
          value: 'armenia',
          label: 'Армения',
        },
        {
          value: 'another',
          label: 'Другие',
        }
      ],
      monthNames: [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь'
      ],
      statuses: [
        {
          value: 'disrtib',
          label: 'Дистрибьютор'
        },
        {
          value: 'success',
          label: 'Success Builder'
        },
        {
          value: 'nationalsuper',
          label: 'Супервайзор'
        },
        {
          value: 'fullsuper',
          label: 'Активный Супервайзор'
        },
        {
          value: 'worldteam',
          label: 'World Team'
        },
        {
          value: 'getteam',
          label: 'Get Team'
        },
        {
          value: 'millinaire',
          label: 'Millionaire Team'
        },
        {
          value: 'presidents',
          label: 'President\'s Team'
        },
        {
          value: 'chairmans',
          label: 'Chairman\'s Club'
        }
      ]
    }
  }

  public handleSubmit() {
    console.log(this.formGroup.value);
  }

  public gotoLogin() {
    this.navCtrl.setRoot(LoginPage);
  }

}
