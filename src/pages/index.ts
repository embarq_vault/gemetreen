import { DayViewPage } from './day-view/day-view';
import { LoginPage } from './login/login';
import { MonthViewPage } from './month-view/month-view';
import { ProfilePage } from './profile/profile';
import { RegisterPage } from './register/register';
import { SettingsPage } from './settings/settings';
import { WeekViewPage } from './week-view/week-view';

export const pages: Array<any> = [
  DayViewPage,
  LoginPage,
  MonthViewPage,
  ProfilePage,
  RegisterPage,
  SettingsPage,
  WeekViewPage
];
