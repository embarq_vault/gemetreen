import { NgModule, ErrorHandler } from '@angular/core';
import { HttpModule } from '@angular/http';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { AngularFireModule, FirebaseAppConfig } from 'angularfire2';

import { MyApp } from './app.component';
import { pages } from '../pages/index';
import { providers } from '../providers/index';

const errorHandler = { provide: ErrorHandler, useClass: IonicErrorHandler };
const firebaseConfig: FirebaseAppConfig = {
  apiKey: "AIzaSyCw-KSpM-oQwj3dncLjlLhrDwjoP4ALx04",
  authDomain: "gemetreen-aa66b.firebaseapp.com",
  databaseURL: "https://gemetreen-aa66b.firebaseio.com",
  storageBucket: "gemetreen-aa66b.appspot.com"
};

@NgModule({
  bootstrap: [ IonicApp ],
  declarations: [ MyApp, pages ],
  entryComponents: [ MyApp, pages ],
  imports: [
    AngularFireModule.initializeApp(firebaseConfig),
    HttpModule,
    IonicModule.forRoot(MyApp),
    ReactiveFormsModule
  ],
  providers: [ errorHandler, providers ]
})
export class AppModule {}
