import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar, Splashscreen } from 'ionic-native';

import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { MonthViewPage } from '../pages/month-view/month-view';
import { SettingsPage } from '../pages/settings/settings';

export interface IonicPage {
  title: string;
  component: any;
};

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  public rootPage: any = LoginPage;
  public pages: Array<IonicPage>;

  constructor(public platform: Platform) {
    this.initializeApp();
    this.pages = [
      { title: 'Profile', component: ProfilePage },
      { title: 'Month View', component: MonthViewPage },
      { title: 'Settings', component: SettingsPage }
    ];
  }

  initializeApp() {
    this.platform
      .ready()
      .then(() => {
        StatusBar.styleDefault();
        Splashscreen.hide();
      });
  }

  open(page): Promise<any> {
    return this.nav.setRoot(page.component);
  }
}
