import { AuthService } from './auth-service';
import { SettingsService } from './settings-service';
import { ProfileService } from './profile-service';

export const providers = [ AuthService, SettingsService, ProfileService ];
