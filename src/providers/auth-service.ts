import { Injectable } from '@angular/core';
import {
  AngularFire,
  AuthMethods,
  AuthProviders
} from 'angularfire2';

export interface User {
  displayName?: string;
  email: string;
  password: string;
  photoUrl?: string;
  uid: string;
}

@Injectable()
export class AuthService {
  public isAuthenticated: boolean = false;
  public currentUser: User;

  constructor(private af: AngularFire) {
    const user = this.getUser();

    if (user != null) {
      this.currentUser = user;
      this.isAuthenticated = true;
    }

    this.af.auth
      .subscribe(
        user => this.changeState(user),
        err => console.error(err));
  }

  private changeState(user: any = null) {
    if (user) {
      this.isAuthenticated = true;
      this.currentUser = user.auth.providerData[0];
      localStorage.setItem('gemetreen-user', JSON.stringify(this.currentUser));
    } else {
      this.isAuthenticated = false;
      this.currentUser = null;
    }
  }

  private getUser() {
    return JSON.parse(localStorage.getItem('gemetreen-user'));
  }

  public register(user: User) {
    return this.af.auth
      .createUser(user)
      .catch(err => console.error(err));
  }

  public login(user: any) {
    let authConfig: Object = {
      provider: AuthProviders.Password,
      method: AuthMethods.Password
    };

    return this.af.auth.login(user, authConfig);
  }

  public logout(): void {
    return this.af.auth.logout();
  }

}
